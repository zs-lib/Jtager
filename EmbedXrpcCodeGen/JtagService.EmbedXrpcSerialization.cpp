﻿#include"JtagService.h"
#include"JtagService.EmbedXrpcSerialization.h"

//auto code gen ! DO NOT modify this file!
//自动代码生成,请不要修改本文件!

const UInt32Field DrRes_Field_DrLen("DrRes.DrLen",offsetof(DrRes,DrLen));
const ArrayField DrRes_Field_OutDr("DrRes.OutDr",true,&UInt32TypeInstance,sizeof(UInt32),offsetof(DrRes,OutDr),&DrRes_Field_DrLen);
const IField* DrResDesc []=
{
&DrRes_Field_DrLen,
&DrRes_Field_OutDr,
};
const ObjectType DrRes_Type(sizeof(DrResDesc)/sizeof(IField*),DrResDesc);

const UInt32Field IrRes_Field_IrLen("IrRes.IrLen",offsetof(IrRes,IrLen));
const ArrayField IrRes_Field_OutIr("IrRes.OutIr",true,&UInt32TypeInstance,sizeof(UInt32),offsetof(IrRes,OutIr),&IrRes_Field_IrLen);
const IField* IrResDesc []=
{
&IrRes_Field_IrLen,
&IrRes_Field_OutIr,
};
const ObjectType IrRes_Type(sizeof(IrResDesc)/sizeof(IField*),IrResDesc);

const UInt32Field Inter_SetSpeed_Request_Field_Speed("Inter_SetSpeed_Request.Speed",offsetof(Inter_SetSpeed_Request,Speed));
const IField* Inter_SetSpeed_RequestDesc []=
{
&Inter_SetSpeed_Request_Field_Speed,
};
const ObjectType Inter_SetSpeed_Request_Type(sizeof(Inter_SetSpeed_RequestDesc)/sizeof(IField*),Inter_SetSpeed_RequestDesc);

const UInt8Field Inter_SetSpeed_Response_Field_State("Inter_SetSpeed_Response.State",offsetof(Inter_SetSpeed_Response,State));
const UInt8Field Inter_SetSpeed_Response_Field_ReturnValue("Inter_SetSpeed_Response.ReturnValue",offsetof(Inter_SetSpeed_Response,ReturnValue));
const IField* Inter_SetSpeed_ResponseDesc []=
{
&Inter_SetSpeed_Response_Field_State,
&Inter_SetSpeed_Response_Field_ReturnValue,
};
const ObjectType Inter_SetSpeed_Response_Type(sizeof(Inter_SetSpeed_ResponseDesc)/sizeof(IField*),Inter_SetSpeed_ResponseDesc);

const UInt32Field Inter_TapShiftDr_Request_Field_DrLen("Inter_TapShiftDr_Request.DrLen",offsetof(Inter_TapShiftDr_Request,DrLen));
const ArrayField Inter_TapShiftDr_Request_Field_InDr("Inter_TapShiftDr_Request.InDr",true,&UInt32TypeInstance,sizeof(UInt32),offsetof(Inter_TapShiftDr_Request,InDr),&Inter_TapShiftDr_Request_Field_DrLen);
const IField* Inter_TapShiftDr_RequestDesc []=
{
&Inter_TapShiftDr_Request_Field_DrLen,
&Inter_TapShiftDr_Request_Field_InDr,
};
const ObjectType Inter_TapShiftDr_Request_Type(sizeof(Inter_TapShiftDr_RequestDesc)/sizeof(IField*),Inter_TapShiftDr_RequestDesc);

const UInt8Field Inter_TapShiftDr_Response_Field_State("Inter_TapShiftDr_Response.State",offsetof(Inter_TapShiftDr_Response,State));
const ObjectField Inter_TapShiftDr_Response_Field_ReturnValue("Inter_TapShiftDr_Response.ReturnValue",sizeof(DrResDesc)/sizeof(IField*),DrResDesc,offsetof(Inter_TapShiftDr_Response,ReturnValue));
const IField* Inter_TapShiftDr_ResponseDesc []=
{
&Inter_TapShiftDr_Response_Field_State,
&Inter_TapShiftDr_Response_Field_ReturnValue,
};
const ObjectType Inter_TapShiftDr_Response_Type(sizeof(Inter_TapShiftDr_ResponseDesc)/sizeof(IField*),Inter_TapShiftDr_ResponseDesc);

const UInt32Field Inter_TapShiftIr_Request_Field_IrLen("Inter_TapShiftIr_Request.IrLen",offsetof(Inter_TapShiftIr_Request,IrLen));
const ArrayField Inter_TapShiftIr_Request_Field_InIr("Inter_TapShiftIr_Request.InIr",true,&UInt32TypeInstance,sizeof(UInt32),offsetof(Inter_TapShiftIr_Request,InIr),&Inter_TapShiftIr_Request_Field_IrLen);
const IField* Inter_TapShiftIr_RequestDesc []=
{
&Inter_TapShiftIr_Request_Field_IrLen,
&Inter_TapShiftIr_Request_Field_InIr,
};
const ObjectType Inter_TapShiftIr_Request_Type(sizeof(Inter_TapShiftIr_RequestDesc)/sizeof(IField*),Inter_TapShiftIr_RequestDesc);

const UInt8Field Inter_TapShiftIr_Response_Field_State("Inter_TapShiftIr_Response.State",offsetof(Inter_TapShiftIr_Response,State));
const ObjectField Inter_TapShiftIr_Response_Field_ReturnValue("Inter_TapShiftIr_Response.ReturnValue",sizeof(IrResDesc)/sizeof(IField*),IrResDesc,offsetof(Inter_TapShiftIr_Response,ReturnValue));
const IField* Inter_TapShiftIr_ResponseDesc []=
{
&Inter_TapShiftIr_Response_Field_State,
&Inter_TapShiftIr_Response_Field_ReturnValue,
};
const ObjectType Inter_TapShiftIr_Response_Type(sizeof(Inter_TapShiftIr_ResponseDesc)/sizeof(IField*),Inter_TapShiftIr_ResponseDesc);

const ObjectType Inter_TapGoIdle_Request_Type(0,nullptr);

const UInt8Field Inter_TapGoIdle_Response_Field_State("Inter_TapGoIdle_Response.State",offsetof(Inter_TapGoIdle_Response,State));
const ObjectField Inter_TapGoIdle_Response_Field_ReturnValue("Inter_TapGoIdle_Response.ReturnValue",sizeof(IrResDesc)/sizeof(IField*),IrResDesc,offsetof(Inter_TapGoIdle_Response,ReturnValue));
const IField* Inter_TapGoIdle_ResponseDesc []=
{
&Inter_TapGoIdle_Response_Field_State,
&Inter_TapGoIdle_Response_Field_ReturnValue,
};
const ObjectType Inter_TapGoIdle_Response_Type(sizeof(Inter_TapGoIdle_ResponseDesc)/sizeof(IField*),Inter_TapGoIdle_ResponseDesc);

const UInt32Field Inter_TapRun_Request_Field_Ticks("Inter_TapRun_Request.Ticks",offsetof(Inter_TapRun_Request,Ticks));
const IField* Inter_TapRun_RequestDesc []=
{
&Inter_TapRun_Request_Field_Ticks,
};
const ObjectType Inter_TapRun_Request_Type(sizeof(Inter_TapRun_RequestDesc)/sizeof(IField*),Inter_TapRun_RequestDesc);

const UInt8Field Inter_Set_TAP_IR_PRE_Request_Field_IrPre("Inter_Set_TAP_IR_PRE_Request.IrPre",offsetof(Inter_Set_TAP_IR_PRE_Request,IrPre));
const IField* Inter_Set_TAP_IR_PRE_RequestDesc []=
{
&Inter_Set_TAP_IR_PRE_Request_Field_IrPre,
};
const ObjectType Inter_Set_TAP_IR_PRE_Request_Type(sizeof(Inter_Set_TAP_IR_PRE_RequestDesc)/sizeof(IField*),Inter_Set_TAP_IR_PRE_RequestDesc);

const UInt8Field Inter_Set_TAP_IR_POST_Request_Field_IrPre("Inter_Set_TAP_IR_POST_Request.IrPre",offsetof(Inter_Set_TAP_IR_POST_Request,IrPre));
const IField* Inter_Set_TAP_IR_POST_RequestDesc []=
{
&Inter_Set_TAP_IR_POST_Request_Field_IrPre,
};
const ObjectType Inter_Set_TAP_IR_POST_Request_Type(sizeof(Inter_Set_TAP_IR_POST_RequestDesc)/sizeof(IField*),Inter_Set_TAP_IR_POST_RequestDesc);

const UInt8Field Inter_Set_TAP_DR_PRE_Request_Field_IrPre("Inter_Set_TAP_DR_PRE_Request.IrPre",offsetof(Inter_Set_TAP_DR_PRE_Request,IrPre));
const IField* Inter_Set_TAP_DR_PRE_RequestDesc []=
{
&Inter_Set_TAP_DR_PRE_Request_Field_IrPre,
};
const ObjectType Inter_Set_TAP_DR_PRE_Request_Type(sizeof(Inter_Set_TAP_DR_PRE_RequestDesc)/sizeof(IField*),Inter_Set_TAP_DR_PRE_RequestDesc);

const UInt8Field Inter_Set_TAP_DR_POST_Request_Field_IrPre("Inter_Set_TAP_DR_POST_Request.IrPre",offsetof(Inter_Set_TAP_DR_POST_Request,IrPre));
const IField* Inter_Set_TAP_DR_POST_RequestDesc []=
{
&Inter_Set_TAP_DR_POST_Request_Field_IrPre,
};
const ObjectType Inter_Set_TAP_DR_POST_Request_Type(sizeof(Inter_Set_TAP_DR_POST_RequestDesc)/sizeof(IField*),Inter_Set_TAP_DR_POST_RequestDesc);

const ObjectType Inter_Reset_Request_Type(0,nullptr);

