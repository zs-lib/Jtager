﻿#include"JtagService.Server.h"
void Inter_SetSpeedService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_SetSpeed_Request request;
Inter_SetSpeed_Request_Type.Deserialize(recManager,&request);
SetSpeed(request.Speed);
Inter_SetSpeed_Request_Type.Free(&request);
Inter_SetSpeed_Response_Type.Serialize(sendManager,0,&Response);
Inter_SetSpeed_Response_Type.Free(&Response);
}
Inter_SetSpeedService Inter_SetSpeedServiceInstance;
void Inter_TapShiftDrService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_TapShiftDr_Request request;
Inter_TapShiftDr_Request_Type.Deserialize(recManager,&request);
TapShiftDr(request.DrLen,request.InDr);
Inter_TapShiftDr_Request_Type.Free(&request);
Inter_TapShiftDr_Response_Type.Serialize(sendManager,0,&Response);
Inter_TapShiftDr_Response_Type.Free(&Response);
}
Inter_TapShiftDrService Inter_TapShiftDrServiceInstance;
void Inter_TapShiftIrService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_TapShiftIr_Request request;
Inter_TapShiftIr_Request_Type.Deserialize(recManager,&request);
TapShiftIr(request.IrLen,request.InIr);
Inter_TapShiftIr_Request_Type.Free(&request);
Inter_TapShiftIr_Response_Type.Serialize(sendManager,0,&Response);
Inter_TapShiftIr_Response_Type.Free(&Response);
}
Inter_TapShiftIrService Inter_TapShiftIrServiceInstance;
void Inter_TapGoIdleService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_TapGoIdle_Request request;
Inter_TapGoIdle_Request_Type.Deserialize(recManager,&request);
TapGoIdle();
Inter_TapGoIdle_Request_Type.Free(&request);
Inter_TapGoIdle_Response_Type.Serialize(sendManager,0,&Response);
Inter_TapGoIdle_Response_Type.Free(&Response);
}
Inter_TapGoIdleService Inter_TapGoIdleServiceInstance;
void Inter_TapRunService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_TapRun_Request request;
Inter_TapRun_Request_Type.Deserialize(recManager,&request);
TapRun(request.Ticks);
Inter_TapRun_Request_Type.Free(&request);
}
Inter_TapRunService Inter_TapRunServiceInstance;
void Inter_Set_TAP_IR_PREService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_Set_TAP_IR_PRE_Request request;
Inter_Set_TAP_IR_PRE_Request_Type.Deserialize(recManager,&request);
Set_TAP_IR_PRE(request.IrPre);
Inter_Set_TAP_IR_PRE_Request_Type.Free(&request);
}
Inter_Set_TAP_IR_PREService Inter_Set_TAP_IR_PREServiceInstance;
void Inter_Set_TAP_IR_POSTService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_Set_TAP_IR_POST_Request request;
Inter_Set_TAP_IR_POST_Request_Type.Deserialize(recManager,&request);
Set_TAP_IR_POST(request.IrPre);
Inter_Set_TAP_IR_POST_Request_Type.Free(&request);
}
Inter_Set_TAP_IR_POSTService Inter_Set_TAP_IR_POSTServiceInstance;
void Inter_Set_TAP_DR_PREService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_Set_TAP_DR_PRE_Request request;
Inter_Set_TAP_DR_PRE_Request_Type.Deserialize(recManager,&request);
Set_TAP_DR_PRE(request.IrPre);
Inter_Set_TAP_DR_PRE_Request_Type.Free(&request);
}
Inter_Set_TAP_DR_PREService Inter_Set_TAP_DR_PREServiceInstance;
void Inter_Set_TAP_DR_POSTService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_Set_TAP_DR_POST_Request request;
Inter_Set_TAP_DR_POST_Request_Type.Deserialize(recManager,&request);
Set_TAP_DR_POST(request.IrPre);
Inter_Set_TAP_DR_POST_Request_Type.Free(&request);
}
Inter_Set_TAP_DR_POSTService Inter_Set_TAP_DR_POSTServiceInstance;
void Inter_ResetService::Invoke(SerializationManager &recManager, SerializationManager& sendManager)
{
static Inter_Reset_Request request;
Inter_Reset_Request_Type.Deserialize(recManager,&request);
Reset();
Inter_Reset_Request_Type.Free(&request);
}
Inter_ResetService Inter_ResetServiceInstance;
RequestMessageMap Inter_RequestMessages[]=
{
{"Inter_SetSpeed",&Inter_SetSpeedServiceInstance},
{"Inter_TapShiftDr",&Inter_TapShiftDrServiceInstance},
{"Inter_TapShiftIr",&Inter_TapShiftIrServiceInstance},
{"Inter_TapGoIdle",&Inter_TapGoIdleServiceInstance},
{"Inter_TapRun",&Inter_TapRunServiceInstance},
{"Inter_Set_TAP_IR_PRE",&Inter_Set_TAP_IR_PREServiceInstance},
{"Inter_Set_TAP_IR_POST",&Inter_Set_TAP_IR_POSTServiceInstance},
{"Inter_Set_TAP_DR_PRE",&Inter_Set_TAP_DR_PREServiceInstance},
{"Inter_Set_TAP_DR_POST",&Inter_Set_TAP_DR_POSTServiceInstance},
{"Inter_Reset",&Inter_ResetServiceInstance},
};
