﻿#ifndef JtagService_Server_H
#define JtagService_Server_H
#include"JtagService.h"
#include"EmbedXrpcServerObject.h"
#include"JtagService.EmbedXrpcSerialization.h"
class Inter_SetSpeedService:public IService
{
public:
uint16_t GetSid(){return Inter_SetSpeed_ServiceId;}
Inter_SetSpeed_Response Response;
void SetSpeed(UInt32 Speed);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_TapShiftDrService:public IService
{
public:
uint16_t GetSid(){return Inter_TapShiftDr_ServiceId;}
Inter_TapShiftDr_Response Response;
void TapShiftDr(UInt32 DrLen,UInt32 InDr[4]);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_TapShiftIrService:public IService
{
public:
uint16_t GetSid(){return Inter_TapShiftIr_ServiceId;}
Inter_TapShiftIr_Response Response;
void TapShiftIr(UInt32 IrLen,UInt32 InIr[4]);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_TapGoIdleService:public IService
{
public:
uint16_t GetSid(){return Inter_TapGoIdle_ServiceId;}
Inter_TapGoIdle_Response Response;
void TapGoIdle();
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_TapRunService:public IService
{
public:
uint16_t GetSid(){return Inter_TapRun_ServiceId;}
void TapRun(UInt32 Ticks);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_Set_TAP_IR_PREService:public IService
{
public:
uint16_t GetSid(){return Inter_Set_TAP_IR_PRE_ServiceId;}
void Set_TAP_IR_PRE(Byte IrPre);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_Set_TAP_IR_POSTService:public IService
{
public:
uint16_t GetSid(){return Inter_Set_TAP_IR_POST_ServiceId;}
void Set_TAP_IR_POST(Byte IrPre);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_Set_TAP_DR_PREService:public IService
{
public:
uint16_t GetSid(){return Inter_Set_TAP_DR_PRE_ServiceId;}
void Set_TAP_DR_PRE(Byte IrPre);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_Set_TAP_DR_POSTService:public IService
{
public:
uint16_t GetSid(){return Inter_Set_TAP_DR_POST_ServiceId;}
void Set_TAP_DR_POST(Byte IrPre);
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
class Inter_ResetService:public IService
{
public:
uint16_t GetSid(){return Inter_Reset_ServiceId;}
void Reset();
void Invoke(SerializationManager &recManager, SerializationManager& sendManager);
};
#define Inter_RequestMessages_Count  10
extern RequestMessageMap Inter_RequestMessages[Inter_RequestMessages_Count];

#endif
