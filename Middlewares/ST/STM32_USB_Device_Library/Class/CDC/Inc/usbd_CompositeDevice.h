/**
  ******************************************************************************
  * @file    usbd_cdc.h
  * @author  MCD Application Team
  * @brief   header file for the usbd_cdc.c file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2015 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                      www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_CDC_H
#define __USB_CDC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include  "usbd_ioreq.h"

/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */

/** @defgroup usbd_cdc
  * @brief This file is the Header file for usbd_cdc.c
  * @{
  */


/** @defgroup usbd_cdc_Exported_Defines
  * @{
  */
#define Icws_Link_IN_EP                                   0x81U  /* EP1 for data IN */
#define Icws_Link_OUT_EP                                  0x01U  /* EP1 for data OUT */

#define Icws_DebugUart_IN_EP                                 0x82U  /*EP2*/
#define Icws_DebugUart_OUT_EP                                0x02U  /*EP2*/

#define Icws_CDC_CMD_EP                                  0x03U  /* EP3 for CDC commands */

#ifndef Icws_UsbDevice_HS_BINTERVAL
#define Icws_UsbDevice_HS_BINTERVAL                          0x10U
#endif /* Icws_UsbDevice_HS_BINTERVAL */

#ifndef Icws_UsbDevice_FS_BINTERVAL
#define Icws_UsbDevice_FS_BINTERVAL                          0x01U
#endif /* Icws_UsbDevice_FS_BINTERVAL */

/* CDC Endpoints parameters: you can fine tune these values depending on the needed baudrates and performance. */
#define Icws_UsbDevice_DATA_HS_MAX_PACKET_SIZE                 512U  /* Endpoint IN & OUT Packet size */
#define Icws_UsbDevice_DATA_FS_MAX_PACKET_SIZE                 64U  /* Endpoint IN & OUT Packet size */
#define Icws_UsbDevice_CMD_PACKET_SIZE                         8U  /* Control Endpoint Packet size */

#define Icws_UsbDevice_CONFIG_DESC_SIZ                     106U
#define Icws_UsbDevice_DATA_HS_IN_PACKET_SIZE                  Icws_UsbDevice_DATA_HS_MAX_PACKET_SIZE
#define Icws_UsbDevice_DATA_HS_OUT_PACKET_SIZE                 Icws_UsbDevice_DATA_HS_MAX_PACKET_SIZE

#define Icws_UsbDevice_DATA_FS_IN_PACKET_SIZE                  Icws_UsbDevice_DATA_FS_MAX_PACKET_SIZE
#define Icws_UsbDevice_DATA_FS_OUT_PACKET_SIZE                 Icws_UsbDevice_DATA_FS_MAX_PACKET_SIZE

/*---------------------------------------------------------------------*/
/*  CDC definitions                                                    */
/*---------------------------------------------------------------------*/
#define DebugUart_SEND_ENCAPSULATED_COMMAND               0x00U
#define DebugUart_GET_ENCAPSULATED_RESPONSE               0x01U
#define DebugUart_SET_COMM_FEATURE                        0x02U
#define DebugUart_GET_COMM_FEATURE                        0x03U
#define DebugUart_CLEAR_COMM_FEATURE                      0x04U
#define DebugUart_SET_LINE_CODING                         0x20U
#define DebugUart_GET_LINE_CODING                         0x21U
#define DebugUart_SET_CONTROL_LINE_STATE                  0x22U
#define DebugUart_SEND_BREAK                              0x23U

/**
  * @}
  */


/** @defgroup USBD_CORE_Exported_TypesDefinitions
  * @{
  */

/**
  * @}
  */
typedef struct
{
  uint32_t bitrate;
  uint8_t  format;
  uint8_t  paritytype;
  uint8_t  datatype;
} USBD_DebugUart_LineCodingTypeDef;
/*
typedef struct _USBD_CDC_Itf
{
  int8_t (* Init)(void);
  int8_t (* DeInit)(void);
  int8_t (* Control)(uint8_t cmd, uint8_t *pbuf, uint16_t length);
  int8_t (* Receive)(uint8_t *Buf, uint32_t *Len);

} USBD_DebugUart_ItfTypeDef;
*/
typedef struct _EpBuffer_t
{
  uint8_t RxBuffer[Icws_UsbDevice_DATA_HS_MAX_PACKET_SIZE];
  uint8_t TxBuffer[Icws_UsbDevice_DATA_HS_MAX_PACKET_SIZE];
  uint32_t RxLength;
  uint32_t TxLength;
  __IO uint32_t TxState;
  __IO uint32_t RxState;
}EpBuffer_t;
typedef struct
{
  uint32_t data[Icws_UsbDevice_DATA_HS_MAX_PACKET_SIZE / 4U];      /* Force 32bits alignment */
  //uint8_t  CmdOpCode;
  //uint8_t  CmdLength;
  //uint8_t  *RxBuffer;
  //uint8_t  *TxBuffer;
  uint8_t Ep3Rxbuffer[8];
  EpBuffer_t EpBuffers[2];
  
  //uint32_t RxLength;
  //uint32_t TxLength;

  //__IO uint32_t TxState;
  //__IO uint32_t RxState;
}USBD_CompositeDeviceClassData_t;



/** @defgroup USBD_CORE_Exported_Macros
  * @{
  */

/**
  * @}
  */

/** @defgroup USBD_CORE_Exported_Variables
  * @{
  */

extern USBD_ClassTypeDef  USBD_CompositeDevice;
#define USBD_CompositeDevice_CLASS    &USBD_CompositeDevice
/**
  * @}
  */

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif  /* __USB_CDC_H */
/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
