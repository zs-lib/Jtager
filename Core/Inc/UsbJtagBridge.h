#ifndef UsbJtagBridge_H
#define UsbJtagBridge_H



class Jtag;
class UsbJtagBridge;
class JtagSetSpeedService;


class JtagServiceRequest
{
public:
	rt_uint8_t CmdId;
	rt_uint8_t *Data;
	rt_uint16_t DataLen;
};

class IJtagService
{
public:
    virtual rt_uint8_t GetCmdId()=0;
	virtual const char *GetServiceName()=0;
	virtual void ServerProcessRequest(UsbJtagBridge *bridger,JtagServiceRequest *req)=0;
};

class UsbJtagBridge
{
public:
	static void UsbJtagBridgeTask(void *arg);
    typedef bool (*SendResponse_t)(UsbJtagBridge *bridge,rt_uint8_t *data,rt_uint16_t dataLen);
    inline void Init(Jtag *jtager,IJtagService* services,rt_uint16_t service_len,SendResponse_t sendResponse)
    {
        this->jtager=jtager;
        this->services=services;
        this->servicesLen=service_len;
        this->sendResponse=sendResponse;
        jtagServiceDataMq=rt_mq_create("JtagServiceDataMq",
                            sizeof(JtagServiceRequest),
                            5,
                            RT_IPC_FLAG_FIFO);
        processMutex=rt_mutex_create("UsbJtagBridgeProcessMutex",RT_IPC_FLAG_FIFO);

        rt_thread_t usb_jtag_bridge=rt_thread_create("usb jtag bridge task",UsbJtagBridgeTask,this,4096,10,10);
        rt_thread_startup(usb_jtag_bridge);
    }
    inline Jtag *GetJtager()
    {
        return jtager;
    }
    inline bool SendResponse(rt_uint8_t *data,rt_uint16_t dataLen)
    {
        if(sendResponse!=nullptr)
            return sendResponse(this,data,dataLen);
        else
            return false;    
    }
private:
    Jtag *jtager=nullptr;
    rt_mq_t jtagServiceDataMq=nullptr;
    IJtagService* services=nullptr;
    rt_uint16_t servicesLen=0;
    rt_mutex_t processMutex=nullptr;
    SendResponse_t sendResponse;
};
extern UsbJtagBridge UsbJtagBridger;



#endif
