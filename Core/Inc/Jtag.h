/*
 * Jtag.h
 *
 *  Created on: Mar 6, 2020
 *      Author: Administrator
 */

#ifndef INC_JTAG_H_
#define INC_JTAG_H_


#define tap_assert(expr) ((void)0U)


class Jtag
{
public:

	void Init();
	bool SetSpeed(uint32_t fkhz);
	void TapShiftDr(uint32_t* old_dr, uint32_t* new_dr, size_t dr_len);
	void TapShiftIr(uint32_t* old_ir, uint32_t* new_ir, size_t ir_len);
	void TapGoIdle(void);
	void TapGoLogicReset(void);
	void TapRun(uint32_t ticks);
	inline void TapSetIrPre(uint8_t pre)
	{
		TARGET_CONFIG_TAP_IR_PRE=pre;
	}
	inline void TapSetIrPost(uint8_t post)
	{
		TARGET_CONFIG_TAP_IR_POST=post;
	}
	inline void TapSetDrPre(uint8_t pre)
	{
		TARGET_CONFIG_TAP_DR_PRE=pre;
	}
	inline void TapSetDrPost(uint8_t post)
	{
		TARGET_CONFIG_TAP_DR_POST=post;
	}
private:

	uint32_t delayNsNumber=5;
	inline uint8_t TapTick(uint8_t tms,uint8_t tdi);
	void TapShift(uint32_t* oldp, uint32_t *newp, size_t len, uint8_t pre, size_t post);
	uint8_t TARGET_CONFIG_TAP_IR_PRE=0;
	uint8_t TARGET_CONFIG_TAP_IR_POST=0;
	uint8_t TARGET_CONFIG_TAP_DR_PRE=0;
	uint8_t TARGET_CONFIG_TAP_DR_POST=0;
};

extern Jtag Jtager;



#endif /* INC_JTAG_H_ */
