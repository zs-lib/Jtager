/*
 * Jtag.h
 *
 *  Created on: Mar 6, 2020
 *      Author: Administrator
 */

#ifndef INC_IJTAGPORT_H_
#define INC_IJTAGPORT_H_

#include <main.h>
/*
class IJtagPort
{
public:
	virtual  void Init()=0;
	
	virtual inline  void SetTms(bool value)=0;
	virtual inline void SetTdi(bool value)=0;
	virtual inline void SetTck(bool value)=0;
	virtual inline bool GetTdo()=0;
	virtual inline void Delay(uint32_t ns)=0;
};
*/
#define GPIO_SET(port,pin) port->BSRR=pin
#define GPIO_CLR(port,pin) port->BSRR=(uint32_t)pin << 16u

#define SetTms(value)	if(value){GPIO_SET(TMS_GPIO_Port,TMS_Pin);GPIO_SET(SHADOW_TMS_GPIO_Port,SHADOW_TMS_Pin);} \
						else{GPIO_CLR(TMS_GPIO_Port,TMS_Pin);GPIO_CLR(SHADOW_TMS_GPIO_Port,SHADOW_TMS_Pin);}

#define SetTdi(value)	if(value){GPIO_SET(TDI_GPIO_Port,TDI_Pin);GPIO_SET(SHADOW_TDI_GPIO_Port,SHADOW_TDI_Pin);} \
						else{GPIO_CLR(TDI_GPIO_Port,TDI_Pin);GPIO_CLR(SHADOW_TDI_GPIO_Port,SHADOW_TDI_Pin);}

#define SetTck(value)	if(value){GPIO_SET(TCK_GPIO_Port,TCK_Pin);GPIO_SET(SHADOW_TCK_GPIO_Port,SHADOW_TCK_Pin);} \
						else{GPIO_CLR(TCK_GPIO_Port,TCK_Pin);GPIO_CLR(SHADOW_TCK_GPIO_Port,SHADOW_TCK_Pin);}

inline uint8_t GetTdo()
{
	uint8_t value=(TDO_GPIO_Port->IDR & TDO_Pin);
	if(value!=0)
	{
		GPIO_SET(SHADOW_TDO_GPIO_Port,SHADOW_TDO_Pin);
	}
	else
	{
		GPIO_CLR(SHADOW_TDO_GPIO_Port,SHADOW_TDO_Pin);
	}
	return value!=0?1:0;
}
//#define GetTdo()	(TDO_GPIO_Port->IDR & TDO_Pin)
inline void Delay(uint32_t ns)
{
	while(ns--);
}

#endif /* INC_IJTAGPORT_H_ */
