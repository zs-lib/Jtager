#include <rtthread.h>
#include <cstdint>
#include <IJtagPort.h>
#include <Jtag.h>
#include <UsbJtagBridge.h>
#include <UsbJtagBridgeCmd.h>
class JtagSetSpeedService:public IJtagService
{
public:
    rt_uint8_t GetCmdId()
    {
        return PcMcuCmd_Jtag_SetSpeed;
    }
    const char *GetServiceName()
    {
        return "JtagSetSpeedService";
    }
    void ServerProcessRequest(UsbJtagBridge *bridger,JtagServiceRequest *req)
    {
        Jtag *jtager=bridger->GetJtager();
        rt_uint16_t speed=(rt_uint16_t)(req->Data[0]|req->Data[1]<<8);
        jtager->SetSpeed(speed);

        rt_uint8_t response[1]={PcMcuCmd_Jtag_SetSpeed+0x40};
        bridger->SendResponse(response,1);        
    }
};
